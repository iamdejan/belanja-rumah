# Belanja Rumah

## Realistis

### Tapak

- https://www.rumah123.com/properti/tangerang/hos9453823/
- https://www.rumah123.com/properti/jakarta-timur/hos10264233/
- https://www.rumah123.com/properti/bekasi/hos9802929/
- https://www.rumah123.com/properti/bogor/hos10037187/
- https://www.dotproperty.id/rumah-dijual-dengan-2-kamar-tidur-di-serpong-banten_6906665
- http://www.olx.co.id/item/rumah-dijual-malibu-gading-serpong-iid-863982690
- http://www.olx.co.id/item/rumah-dijual-graha-raya-iid-864187186
- https://www.rumah123.com/properti/tangerang/hos9054083/
- https://www.rumah123.com/properti/tangerang/hos10046182/
- https://www.rumah123.com/properti/tangerang/shs3130377/
- https://www.rumah123.com/properti/tangerang/hos9911854/
- https://www.rumah123.com/properti/tangerang/hos7695841/
- https://www.olx.co.id/item/bintaro-jaya-rumah-cantik-dalam-cluster-iid-853193029
- https://www.rumah123.com/properti/jakarta-barat/hos4755469/
- https://www.rumah123.com/properti/surabaya/hos12923523/
- https://www.rumah123.com/properti/surabaya/hos13350056/
- https://www.rumah123.com/properti/surabaya/aps2906592/

### Apartemen

- https://www.rumah.com/listing-properti/dijual-south-quarter-oleh-aulia-syafei-rivai-17278518
- https://www.rumah.com/listing-properti/dijual-south-quarter-residence-sudah-topping-off-masih-ada-unit-pilihan-oleh-eddy-pop-17021009
- https://www.rumah123.com/properti/tangerang/aps2873482/
- https://www.rumah123.com/properti/tangerang/aps2895344/
- https://www.rumah123.com/properti/jakarta-selatan/aps2863419/
- https://www.rumah123.com/properti/sidoarjo/aps2668282/
- https://www.rumah123.com/properti/surabaya/aps2807809/
- https://www.rumah123.com/properti/surabaya/aps2855921/
- https://www.rumah123.com/properti/surabaya/aps2877907/
- https://www.rumah123.com/properti/surabaya/aps2906640/

### Ruko

- https://www.rumah123.com/properti/tangerang/shs3092947/
- https://www.rumah123.com/properti/tangerang/shs3171468/
- https://www.rumah123.com/properti/tangerang/shs3185124/
- https://www.rumah123.com/perumahan-baru/properti/tangerang-selatan/ruko-golden-madrid-x/tipe-f/shs5426/
- https://www.rumah123.com/properti/tangerang-selatan/shs3110390/
- https://www.rumah123.com/properti/tangerang/shs2959138/
- https://www.olx.co.id/item/bintaro-jaya-rumah-ruko-kaveling-kebayoran-discovery-uville-iid-859752192
- https://www.rumah123.com/properti/tangerang/shs3290016/#
- https://www.rumah123.com/properti/tangerang/shs2938490/

## Impian

- https://www.rumah123.com/properti/jakarta-selatan/hos8642265/
- https://www.rumah123.com/properti/tangerang/hos8800951/
- https://www.rumah123.com/properti/bogor/hos10313568/
- https://www.rumah123.com/properti/jakarta-utara/hos1634196/
- https://www.rumah123.com/properti/bogor/hos9587349/
- https://www.rumah123.com/properti/tangerang/hos10306721/
- https://www.rumah123.com/properti/jakarta-selatan/hos9776127/
